using UnityEngine;
using UnityEngine.UI;

public class PictureCanvas : MonoBehaviour
{
    public enum ViewMode
    {
        Draw,
        Preview
    }

    public ViewMode viewMode;

    public RectTransform rectTransformPictureCanvas;
    public ColorPickerMenu colorPickerMenu;
    public TileSelectMenu tileSelectMenu;
    public SavePictureMenu savePictureMenu;

    public RawImage mainImage;
    public Texture2D mainTexture;

    private Vector2 mouseDelta;
    private float mousePositionX, mousePositionY;
    private float canvasWidth, canvasHeight;
    private int textureX, textureY = -1;
    private int prevTextureX, prevTextureY = -1;
    private Color previousTileColor;

    private void Update()
    {
        if (viewMode != ViewMode.Draw)
            return;

        if (Input.GetMouseButton(0))
        {
            if (!RectTransformUtility.RectangleContainsScreenPoint(rectTransformPictureCanvas, Input.mousePosition))
            {
                RevertPreviousPixel();
                prevTextureX = -1;
                prevTextureY = -1;
                return;
            }

            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransformPictureCanvas, Input.mousePosition, null, out mouseDelta);

            canvasWidth = rectTransformPictureCanvas.rect.width;
            canvasHeight = rectTransformPictureCanvas.rect.height;

            mouseDelta += new Vector2(canvasWidth * 0.5f, canvasHeight * 0.5f);

            mousePositionX = Mathf.Clamp(mouseDelta.x / canvasWidth, 0f, 1f);
            mousePositionY = Mathf.Clamp(mouseDelta.y / canvasHeight, 0f, 1f);

            textureX = (int)(mousePositionX * mainTexture.width);
            textureY = (int)(mousePositionY * mainTexture.height);

            if (textureX == prevTextureX && textureY == prevTextureY)
                return;

            if (prevTextureX != -1 && prevTextureY != -1)
            {
                mainTexture.SetPixel(prevTextureX, prevTextureY, previousTileColor);
            }

            previousTileColor = mainTexture.GetPixel(textureX, textureY);

            prevTextureX = textureX;
            prevTextureY = textureY;

            mainTexture.SetPixel(textureX, textureY, colorPickerMenu.tileColor);
            mainTexture.Apply();
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (GameManager.Instance.localSaveData.tileAvailable && RectTransformUtility.RectangleContainsScreenPoint(rectTransformPictureCanvas, Input.mousePosition))
            {
                GameManager.Instance.SetTileAvailable(false);
                GameManager.Instance.SetTileRefreshTime(GameManager.Instance.dateTimeNow.Ticks + GameManager.Instance.tileCooldownInMilliseconds);
                GameManager.Instance.localSaveData.currentPicture.data = mainTexture.EncodeToPNG();
                GameManager.Instance.localSaveData.colorRed = colorPickerMenu.tileColor.r;
                GameManager.Instance.localSaveData.colorGreen = colorPickerMenu.tileColor.g;
                GameManager.Instance.localSaveData.colorBlue = colorPickerMenu.tileColor.b;
                GameManager.Instance.UpdateSaveData(GameManager.Instance.localSaveData);

                tileSelectMenu.UpdateClockText();

                if (IsPictureComplete())
                {
                    savePictureMenu.gameObject.SetActive(true);
                }
            }
            else
            {
                RevertPreviousPixel();
            }

            prevTextureX = -1;
            prevTextureY = -1;
        }
    }

    public void RefreshCanvas(PictureData pictureData)
    {
        var currentPicture = pictureData;

        mainTexture = new Texture2D(currentPicture.pictureSize, currentPicture.pictureSize)
        {
            filterMode = FilterMode.Point
        };

        if (currentPicture.data == null || currentPicture.data.Length == 0)
        {
            FillTextureWithColor(new Color(0, 0, 0, 0));
        }
        else
        {
            mainTexture.LoadImage(currentPicture.data);
            mainTexture.Apply();
        }

        mainImage.texture = mainTexture;
    }

    public void SetViewMode(int mode)
    {
        viewMode = (ViewMode)mode;
        tileSelectMenu.gameObject.SetActive(viewMode == ViewMode.Draw);
        colorPickerMenu.gameObject.SetActive(viewMode == ViewMode.Draw);
    }

    private void RevertPreviousPixel()
    {
        if (prevTextureX != -1 && prevTextureY != -1)
        {
            mainTexture.SetPixel(prevTextureX, prevTextureY, previousTileColor);
            mainTexture.Apply();
        }
    }

    private void FillTextureWithColor(Color color)
    {
        var fillColorArray = new Color[mainTexture.width * mainTexture.height];

        for (var i = 0; i < fillColorArray.Length; ++i)
        {
            fillColorArray[i] = color;
        }

        mainTexture.SetPixels(fillColorArray);
        mainTexture.Apply();
    }

    private bool IsPictureComplete()
    {
        var pixels = mainTexture.GetPixels();
        for (var i = 0; i < pixels.Length; ++i)
        {
            if (pixels[i].a == 0)
                return false;
        }

        return true;
    }
}
