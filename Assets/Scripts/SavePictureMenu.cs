﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SavePictureMenu : MonoBehaviour
{
    public InputField inputFieldPictureName;
    public Button buttonSaveAndBack;

    private void OnEnable()
    {
        UpdateButtons();
    }

    public void SavePicture()
    {
        if (string.IsNullOrEmpty(inputFieldPictureName.text))
            return;

        var pictureData = new PictureData
        {
            name = inputFieldPictureName.text,
            pictureSize = GameManager.Instance.localSaveData.currentPicture.pictureSize,
            data = GameManager.Instance.localSaveData.currentPicture.data
        };
        
        var newSavedPictures = new PictureData[GameManager.Instance.localSaveData.savedPictures.Length + 1];

        Array.Copy(GameManager.Instance.localSaveData.savedPictures, newSavedPictures, GameManager.Instance.localSaveData.savedPictures.Length);
        var pictureIndex = newSavedPictures.Length - 1;
        newSavedPictures[pictureIndex] = pictureData;
        GameManager.Instance.localSaveData.savedPictures = newSavedPictures;
        GameManager.Instance.UpdateSaveData(GameManager.Instance.localSaveData);
    }

    public void UpdateButtons()
    {
        buttonSaveAndBack.interactable = inputFieldPictureName.text.Length > 0;
    }
}
