using UnityEngine;
using UnityEngine.UI;

public class TileSelectMenu : MonoBehaviour
{
    public float timerUpdateDelay = 0.5f;

    public Text textClock;
    public GameObject buttonReduceTime;

    private float timer;

    private void Start()
    {
        UpdateClockText();

        GameManager.Instance.EventNewTileAvailable += UpdateClockText;
        GameManager.Instance.EventTileTimeChanged += UpdateClockText;
    }

    private void OnDestroy()
    {
        GameManager.Instance.EventNewTileAvailable -= UpdateClockText;
        GameManager.Instance.EventTileTimeChanged -= UpdateClockText;
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (timer >= timerUpdateDelay)
        {
            UpdateClockText();
            timer = 0;
        }
    }

    public void UpdateClockText()
    {
        var dateTimeNow = GameManager.Instance.dateTimeNow;

        if (dateTimeNow.Ticks < GameManager.Instance.localSaveData.tileRefreshTime)
        {
            buttonReduceTime.SetActive(true);
            var nextTileDate = new System.DateTime(GameManager.Instance.localSaveData.tileRefreshTime);
            var timeDifference = nextTileDate.Subtract(dateTimeNow);

            textClock.text = $"{timeDifference.Days}:{timeDifference.Hours}:{timeDifference.Minutes}:{timeDifference.Seconds}";
        }
        else
        {
            buttonReduceTime.SetActive(false);
            textClock.text = $"TILE AVAILABLE!";
        }
    }
}
