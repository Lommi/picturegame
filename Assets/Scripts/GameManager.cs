using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public const string SAVEDATA_FILENAME = "/savedata.json";
    public string SAVEDATA_FILEPATH;

    public SaveData localSaveData;
    public DateTime dateTimeNow;

    public UnityAction EventNewTileAvailable;
    public UnityAction EventTileTimeChanged;

    public long tileCooldownInMilliseconds;
    public long buttonCooldownReduce;

    public const int DEFAULT_CANVAS_SIZE = 4;
    private const int TARGET_FPS = 60;

    private void Awake()
    {
        Application.targetFrameRate = TARGET_FPS;

        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Debug.LogError("GameManager instance already exists, destroying object!");
            Destroy(this);
        }

        Debug.Log(Application.persistentDataPath);

        DontDestroyOnLoad(gameObject);

        SAVEDATA_FILEPATH = Application.persistentDataPath + SAVEDATA_FILENAME;

        localSaveData = LoadSaveData();

        UpdateDateTimeNow();
    }

    private void Update()
    {
        UpdateDateTimeNow();

        // Could add other handler updates here
    }

    public void UpdateSaveData(SaveData data)
    {
        var saveDataJson = JsonUtility.ToJson(data);
        System.IO.File.WriteAllText(SAVEDATA_FILEPATH, saveDataJson);
    }

    public void DeleteSavedata()
    {
        if (System.IO.File.Exists(SAVEDATA_FILEPATH))
        {
            System.IO.File.Delete(SAVEDATA_FILEPATH);
        }
    }

    public void SetTileRefreshTime(long value)
    {
        localSaveData.tileRefreshTime = value;
        UpdateSaveData(localSaveData);

        EventTileTimeChanged?.Invoke();

        if (value < dateTimeNow.Ticks)
        {
            SetTileAvailable(true);
        }
    }

    public void SetTileAvailable(bool value)
    {
        localSaveData.tileAvailable = value;
        UpdateSaveData(localSaveData);

        EventNewTileAvailable?.Invoke();
    }

    public void OnClickCooldownReduceButton() => SetTileRefreshTime(localSaveData.tileRefreshTime + buttonCooldownReduce);

    public void DeletePicture(PictureData pictureData)
    {
//        var newPictureList = new List<PictureData>();
//        for (var i = 0; i < localSaveData.savedPictures.Length; ++i)
//        {
//            if (localSaveData.savedPictures[i].name == pictureData.name)
//                continue;
//
//            newPictureList.Add(localCustomLevelsData.levelConfigs[i]);
//        }
//
//        localCustomLevelsData.levelConfigs = newPictureList.ToArray();
//        UpdateCustomLevelsData(localCustomLevelsData);
//        InitializeCustomLevels();
    }

    private SaveData LoadSaveData()
    {
        var saveDataJson = string.Empty;

        if (System.IO.File.Exists(SAVEDATA_FILEPATH))
        {
            saveDataJson = System.IO.File.ReadAllText(SAVEDATA_FILEPATH);
        }

        if (string.IsNullOrEmpty(saveDataJson))
        {
            var defaultSaveData = new SaveData
            {
                tileAvailable = true,
                colorRed = 0,
                colorGreen = 0,
                colorBlue = 0,
                savedPictures = new PictureData[0]
            };
            UpdateSaveData(defaultSaveData);

            return defaultSaveData;
        }

        return JsonUtility.FromJson<SaveData>(saveDataJson);
    }

    private void UpdateDateTimeNow()
    {
        if (!localSaveData.tileAvailable && dateTimeNow.Ticks < localSaveData.tileRefreshTime && DateTime.Now.Ticks >= localSaveData.tileRefreshTime)
        {
            SetTileAvailable(true);
        }

        dateTimeNow = DateTime.Now;
    }
}
