using UnityEngine;
using UnityEngine.UI;

public class ColorPickerMenu : MonoBehaviour
{
    public InputField inputFieldColorRed;
    public InputField inputFieldColorGreen;
    public InputField inputFieldColorBlue;
    public Slider sliderColorRed;
    public Slider sliderColorGreen;
    public Slider sliderColorBlue;

    public Image imageTilePreview;

    public Color tileColor;

    private void Start()
    {
        UpdateSliderValues();
        UpdateInputFieldTexts();
        SetTilePreviewColor(float.Parse(inputFieldColorRed.text), float.Parse(inputFieldColorGreen.text), float.Parse(inputFieldColorBlue.text));
    }

    public void OnSliderEdit()
    {
        SetTilePreviewColor(sliderColorRed.value, sliderColorGreen.value, sliderColorBlue.value);
        inputFieldColorRed.text = sliderColorRed.value.ToString();
        inputFieldColorGreen.text = sliderColorGreen.value.ToString();
        inputFieldColorBlue.text = sliderColorBlue.value.ToString();
    }

    public void OnEndEditTileColor()
    {
        SetTilePreviewColor(float.Parse(inputFieldColorRed.text), float.Parse(inputFieldColorGreen.text), float.Parse(inputFieldColorBlue.text));
        UpdateSliderValues();
    }

    public void SetTilePreviewColor(float r, float g, float b)
    {
        tileColor = new Color(r, g, b);
        imageTilePreview.color = tileColor;
    }

    private void UpdateSliderValues()
    {
        sliderColorRed.value = GameManager.Instance.localSaveData.colorRed;
        sliderColorGreen.value = GameManager.Instance.localSaveData.colorGreen;
        sliderColorBlue.value = GameManager.Instance.localSaveData.colorBlue;
    }

    private void UpdateInputFieldTexts()
    {
        inputFieldColorRed.text = GameManager.Instance.localSaveData.colorRed.ToString();
        inputFieldColorGreen.text = GameManager.Instance.localSaveData.colorGreen.ToString();
        inputFieldColorBlue.text = GameManager.Instance.localSaveData.colorBlue.ToString();
    }
}
