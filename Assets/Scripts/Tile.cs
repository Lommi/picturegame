using UnityEngine;
using UnityEngine.UI;

public enum TileType
{
    empty,
    common
}

public class Tile : MonoBehaviour
{
    public TileType type;
    public Image image;

    public void SetImageSprite(Sprite sprite) => image.sprite = sprite;

    public void SetImageColor(Color color) => image.color = color;
}
