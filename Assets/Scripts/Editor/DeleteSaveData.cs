using UnityEditor;
using UnityEngine;

public class DeleteSaveData : MonoBehaviour
{
    [MenuItem("Tools/Delete SaveData")]
    public static void Delete()
    {
        var savedataPath = Application.persistentDataPath + "/savedata.json";

        if (System.IO.File.Exists(savedataPath))
        {
            System.IO.File.Delete(savedataPath);
            Debug.Log($"DELETED {savedataPath}");
        }
        else
        {
            Debug.LogError($"FILE NOT FOUND {savedataPath}");
        }
    }
}