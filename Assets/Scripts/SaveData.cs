[System.Serializable]
public struct SaveData
{
    public bool tileAvailable;
    public float colorRed;
    public float colorGreen;
    public float colorBlue;

    public long tileRefreshTime; // DateTime when new tile is available in ticks

    public PictureData currentPicture;
    public PictureData[] savedPictures;
}

[System.Serializable]
public struct PictureData
{
    public string name;
    public int pictureSize;
    public byte[] data;
}