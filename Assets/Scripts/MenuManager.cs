﻿using UnityEngine;

public enum MenuState
{
    PictureView,
    GalleryView
}

public class MenuManager : MonoBehaviour
{
    public static MenuManager Instance { get; private set; }

    public MenuState menuState;
    public PictureCanvas pictureCanvas;
    public GameObject[] menuObjects;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Debug.LogError("MenuManager instance already exists, destroying object!");
            Destroy(this);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        if ((GameManager.Instance.localSaveData.currentPicture.data == null || GameManager.Instance.localSaveData.currentPicture.data.Length == 0) &&
            (GameManager.Instance.localSaveData.savedPictures.Length == 0))
        {
            GameManager.Instance.localSaveData.currentPicture = new PictureData
            {
                pictureSize = GameManager.DEFAULT_CANVAS_SIZE
            };
        }

        DrawImage();
    }

    public void SetMenuState(int state)
    {
        for (var i = 0; i < menuObjects.Length; ++i)
        {
            menuObjects[i].SetActive(i == state);
        }
    }

    public void DrawImage()
    {
        SetMenuState((int)MenuState.PictureView);
        pictureCanvas.SetViewMode((int)PictureCanvas.ViewMode.Draw);
        pictureCanvas.RefreshCanvas(GameManager.Instance.localSaveData.currentPicture);
    }

    public void PreviewImage(int savedPictureIndex)
    {
        SetMenuState((int)MenuState.PictureView);
        pictureCanvas.SetViewMode((int)PictureCanvas.ViewMode.Preview);
        pictureCanvas.RefreshCanvas(GameManager.Instance.localSaveData.savedPictures[savedPictureIndex]);
    }
}
