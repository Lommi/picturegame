﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PictureButton : MonoBehaviour
{
    public int pictureIndex;

    public Text textPictureName;
    public Button button;
    public RawImage imagePicturePreview;

    public void SetPictureNameText(string text) => textPictureName.text = text;
}
