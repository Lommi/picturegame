﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GalleryViewController : MonoBehaviour
{
    public GameObject createPicturePanel;
    public InputField inputFieldPictureSize;
    public Transform transformPictureButton;
    public PictureButton prefabPictureButton;
    public List<PictureButton> pictureButtons = new List<PictureButton>();

    public void UpdateSavedPicturesButtons()
    {
        var savedPictures = GameManager.Instance.localSaveData.savedPictures;

        for (var i = 0; i < savedPictures.Length; ++i)
        {
            var index = i;

            if (i < pictureButtons.Count)
            {
                var pictureButton = pictureButtons[i];
                pictureButton.pictureIndex = i;
                pictureButton.SetPictureNameText(savedPictures[i].name);
                pictureButton.button.onClick.RemoveAllListeners();
                pictureButton.button.onClick.AddListener(() => OnClickPictureButton(index));
            }
            else
            {
                var pictureData = savedPictures[i];
                var pictureButton = Instantiate(prefabPictureButton, transformPictureButton);
                pictureButton.pictureIndex = i;

                var texture = new Texture2D(pictureData.pictureSize, pictureData.pictureSize)
                {
                    filterMode = FilterMode.Point
                };

                texture.LoadImage(pictureData.data);
                texture.Apply();

                pictureButton.imagePicturePreview.texture = texture;
                pictureButton.SetPictureNameText(pictureData.name);
                pictureButton.button.onClick.RemoveAllListeners();
                pictureButton.button.onClick.AddListener(() => OnClickPictureButton(index));

                pictureButtons.Add(pictureButton);
            }
        }
    }

    public void CreateNewPicture()
    {
        GameManager.Instance.localSaveData.currentPicture = new PictureData
        {
            pictureSize = int.Parse(inputFieldPictureSize.text)
        };

        GameManager.Instance.UpdateSaveData(GameManager.Instance.localSaveData);
        MenuManager.Instance.DrawImage();
    }

    private void OnClickPictureButton(int savedPictureIndex)
    {
        MenuManager.Instance.PreviewImage(savedPictureIndex);
    }
}
